<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductLine", mappedBy="shopping_cart", orphanRemoval=true, cascade={"persist"})
     */
    private $productLines;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="shopping_cart")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OrderHistory", inversedBy="shoppingCart", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $order_history;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    public function __construct()
    {
        $this->productLines = new ArrayCollection();
        $this->active = false;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Collection|ProductLine[]
     */
    public function getProductLines(): Collection
    {
        return $this->productLines;
    }

    public function addProductLine(ProductLine $productLine): self
    {
        if (!$this->productLines->contains($productLine)) {
            $this->productLines[] = $productLine;
            $productLine->setShoppingCart($this);
        }

        return $this;
    }

    public function removeProductLine(ProductLine $productLine): self
    {
        if ($this->productLines->contains($productLine)) {
            $this->productLines->removeElement($productLine);
            // set the owning side to null (unless already changed)
            if ($productLine->getShoppingCart() === $this) {
                $productLine->setShoppingCart(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getOrderHistory(): ?OrderHistory
    {
        return $this->order_history;
    }

    public function setOrderHistory(OrderHistory $order_history): self
    {
        $this->order_history = $order_history;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
