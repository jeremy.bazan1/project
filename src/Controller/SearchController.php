<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProductRepository;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="search")
     */
    public function index(ProductRepository $repo, Request $request)
    {
        if ($request->isMethod('POST')) {
            $products = [];
            $prods = $repo->findAllByName($request->get("nameProduct"));
                forEach($prods as $product) {
                    if(!$product->getRemove()) {
                        $products[] = $product;
                    }
            }
            dump($products);
            return $this->render("product/index.html.twig", ["products" => $products,
            "imageURI" => $this->getParameter('images_URI'),]);
        }

        return $this->render('search/index.html.twig', []);
    }
}
