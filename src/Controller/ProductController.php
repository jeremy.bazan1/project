<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Service\FileUploader;
use App\Form\ProductType;
use App\Repository\CategoryRepository;
use App\Entity\Category;

class ProductController extends Controller
{
    /**
     * @Route("/product", name="product")
     */
    public function index(ProductRepository $repo, Request $request)
    {

        $products = [];

        forEach($repo->findAll() as $product) {
            if(!$product->getRemove()) {
                $products[] = $product;
            }
        }

        if ($request->isMethod('POST')) {
            $products = [];
            $prods = $this->getDoctrine()
                ->getRepository(Product::class)
                ->findAllByName($request->get("nameProduct"));
                forEach($prods as $product) {
                    if(!$product->getRemove()) {
                        $products[] = $product;
                    }
                }
        }

        
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            "products" => $products,
            "imageURI" => $this->getParameter('images_URI'),
        ]);
    }

    /**
     *  @Route("/admin/create-product", name="create_product")
     *  @Route("/admin/{id}/edit-product", name="edit_product")
     */
    public function form(Product $product = null, Request $req, ObjectManager $manager, FileUploader $fileUploader, ProductRepository $repoProd, CategoryRepository $repoCat){

        if(!$product) {
            $product = new Product();
        }

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($req);

        

        if($form->isSubmitted() && $form->isValid()) {

            $file = $product->getImage();

            $fileName = $fileUploader->upload($file);

            $product->setImage($fileName);

            $product->setRemove(false);

            $manager->persist($product);
            $manager->flush();

            return $this->redirectToRoute("product", ["id" => $product->getId()]);
        }
        
        return $this->render("product/product.html.twig", ["formProduct" => $form->createView(),
        'editMode' => $product->getId() ==! null,
        ]);
    }

    /**
     *  @Route("/product/{id}", name="show_product")
     */
    public function show(Product $product, Request $request){

        if ($request->isMethod('POST')) {

            $quantity = abs($request->get("quantity"));
    
            return $this->redirectToRoute("product_line", ["id" => $product->getId(), "quantity" => $quantity]);
        }

        return $this->render("product/show.html.twig", ["product" => $product, "imageURI" => $this->getParameter('images_URI')]);
    }

    /**
    *  @Route("/admin/{id}/remove-product", name="remove_product")
    */
    public function remove(Product $product, ObjectManager $manager) {

        // $em = $this->getDoctrine()->getEntityManager();
        // $em->remove($product);
        // $em->flush();

        $product->setRemove(true);

        $manager->persist($product);
        $manager->flush();

        return $this->redirectToRoute("product", []);
    }

    /**
     * @Route("/product/search", name="product_search")
     */
    
}
