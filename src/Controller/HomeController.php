<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/admin/getNbItems", name="cartItems")
     */
    public function getNbCartItems(SessionInterface $session)
    {

       $shoppingCart = $session->get("cart");

       if($shoppingCart == null) {
           $count = 0;
       }
       else {
           $count = count($shoppingCart->getProductLines());
       }
       
        return $this->render('features/countItem.html.twig', [
            'controller_name' => 'HomeController',
            "count" => $count
        ]);

    }
}
