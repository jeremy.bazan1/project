<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180731145017 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_line (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, shopping_cart_id INT NOT NULL, quantity INT NOT NULL, price INT NOT NULL, INDEX IDX_5CFC96574584665A (product_id), INDEX IDX_5CFC965745F80CD (shopping_cart_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_line ADD CONSTRAINT FK_5CFC96574584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_line ADD CONSTRAINT FK_5CFC965745F80CD FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id)');
        $this->addSql('DROP TABLE shopping_cart_product');
        $this->addSql('ALTER TABLE order_history DROP FOREIGN KEY FK_D1C0D90045F80CD');
        $this->addSql('DROP INDEX IDX_D1C0D90045F80CD ON order_history');
        $this->addSql('ALTER TABLE order_history DROP shopping_cart_id, DROP date');
        $this->addSql('ALTER TABLE shopping_cart CHANGE user_id user_id INT DEFAULT NULL, CHANGE quantity order_history_id INT NOT NULL');
        $this->addSql('ALTER TABLE shopping_cart ADD CONSTRAINT FK_72AAD4F6ADDF7907 FOREIGN KEY (order_history_id) REFERENCES order_history (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_72AAD4F6ADDF7907 ON shopping_cart (order_history_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE shopping_cart_product (shopping_cart_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_FA1F5E6C45F80CD (shopping_cart_id), INDEX IDX_FA1F5E6C4584665A (product_id), PRIMARY KEY(shopping_cart_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shopping_cart_product ADD CONSTRAINT FK_FA1F5E6C4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shopping_cart_product ADD CONSTRAINT FK_FA1F5E6C45F80CD FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE product_line');
        $this->addSql('ALTER TABLE order_history ADD shopping_cart_id INT NOT NULL, ADD date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE order_history ADD CONSTRAINT FK_D1C0D90045F80CD FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id)');
        $this->addSql('CREATE INDEX IDX_D1C0D90045F80CD ON order_history (shopping_cart_id)');
        $this->addSql('ALTER TABLE shopping_cart DROP FOREIGN KEY FK_72AAD4F6ADDF7907');
        $this->addSql('DROP INDEX UNIQ_72AAD4F6ADDF7907 ON shopping_cart');
        $this->addSql('ALTER TABLE shopping_cart CHANGE user_id user_id INT NOT NULL, CHANGE order_history_id quantity INT NOT NULL');
    }
}
