# Symfony docker skeleton

## Quick start
1. `composer install`
1. `docker-compose up`
1. go to http://localhost:8080

## SASS with yarn
1. yarn add sass-loader node-sass --dev

## Package manager
1. `npm install`
1. `yarn install`

## Dev daily commands
1. `docker-compose up`
1. `npm run watch` or `yarn encore dev --watch`

